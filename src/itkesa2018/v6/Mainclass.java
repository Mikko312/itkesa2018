/*
 * To change this license header, choose License Headers in Project Properties.
/*
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itkesa2018.v6;

import java.util.Scanner;

/**
 *
 * @author n4121
 */
public class Mainclass {
    private static int valinta = 1;
    private static String number = null;
    private static int money ,limit;
    public static void main(String[] args){
        Bank bank = new Bank();
        while(valinta != 0){
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            Scanner scan = new Scanner(System.in);
            valinta = scan.nextInt();
            switch(valinta){
                case 1:
                    System.out.print("Syötä tilinumero: ");
                    number = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    bank.addAccountT(number, money);
                    break;
                case 2:
                    System.out.print("Syötä tilinumero: ");
                    number = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    System.out.print("Syötä luottoraja: ");
                    limit = scan.nextInt();
                    bank.addAccountL(number, money, limit);
                    break;
                case 3:
                    System.out.print("Syötä tilinumero: ");
                    number = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    bank.addMoney(number, money);
                    break;
                case 4:
                    System.out.print("Syötä tilinumero: ");
                    number = scan.next();
                    System.out.print("Syötä rahamäärä: ");
                    money = scan.nextInt();
                    bank.pickMoney(number, money);
                    break;
                case 5:
                    System.out.print("Syötä poistettava tilinumero: ");
                    number = scan.next();
                    bank.deleteAccount(number);
                    break;
                case 6:
                    System.out.print("Syötä tulostettava tilinumero: ");
                    number = scan.next();
                    bank.searchAccount(number);
                   
                    break;
                case 7:
                    bank.printAccount();
                    break;
                case 0:
                    break;
                default: 
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        }
    }
}
